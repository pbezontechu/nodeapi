var express = require('express'),
  app = express(),
  bodyParser = require('body-parser'),
  port = process.env.PORT || 3000,
  cors = require('cors');

var path = require('path');
var requestjson = require('request-json');

var mongoClient = require('mongodb').MongoClient;
// ojo que esto no funciona en docker, hay que cambiar localhost por el nombre
var url = "mongodb://localhost:27017/local";


// postgre
const pg = require('pg');
// ojo que esto no funciona a docker, hay que cabiar el localhost por el nombre del contenedor
const connectionString = 'postgres://docker:docker@localhost:5433/bdseguridad';

app.listen(port);
app.use(bodyParser.json());
app.use(cors());

console.log('todo list RESTful API server started on: ' + port);

var urlLocationMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/locations/?apiKey=MzPOaTAYuMrwm8jpCLPnmyXjSNFpL8Bq";

/*******************
* Login y registro *
********************/
app.post('/login', function (request, response) {
  //crear cliente postgre
  const clientPg = new pg.Client(connectionString);
  clientPg.connect();
  //hacer consulta
  const query = clientPg.query('SELECT COUNT(*) as encontrados FROM users WHERE username=$1 AND password = crypt($2, password);', [request.body.username, request.body.password],
    (err, result) => {
      if (err) {
        console.log(err);
        response.send(err);
      } else {
        if(result.rows[0].encontrados >= 1) {
            response.send("Login correcto");
        } else {
          response.status(403).send("Login incorrecto");
        }
      }
    });
  //devolver resultado
});


app.post('/register', function (request, response) {
  //crear cliente postgre
  const clientPg = new pg.Client(connectionString);
  clientPg.connect();
  //hacer consulta
  const query = clientPg.query("INSERT INTO users (username, name, surname, email, password) VALUES($1, $2, $3, $4, crypt($5, gen_salt('bf', 8)));", 
    [request.body.username, request.body.name, request.body.surname, request.body.email, request.body.password],
    (err, result) => {
      if (err) {
        console.log(err);
        response.status(500).send(err);
      } else {
        response.send("Registro correcto");
      }
    });
  //devolver resultado
});

/****************
* Cuentas - get *
*****************/
app.get('/accounts', function (req, response) {
  mongoClient.connect(url, function (err, db) {
    var col = db.collection('customer_accounts');
    var query = { customer_id: req.headers.user };
    var filter = { "accounts.accountId": 1 };
    col.find(query, filter).toArray(function (err, results) {
      response.send(results[0]);
    });
    db.close();
  });
});

app.get('/accounts/:id', function (req, response) {
  mongoClient.connect(url, function (err, db) {
    var col = db.collection('customer_accounts');
    var query = { customer_id: req.headers.user };
    var filter = { accounts: { $elemMatch: { accountId: req.params.id } } };
    col.findOne(query, filter, function (err, docs) {
      response.send(docs);
    });
    db.close();
  });
});

app.get('/accounts/:id/balance', function (req, response) {
  mongoClient.connect(url, function (err, db) {
    var col = db.collection('customer_accounts');
    var query = [
      { $match: { customer_id: req.headers.user } },
      { $unwind: '$accounts' },
      { $match: { 'accounts.accountId': req.params.id } },
      { $unwind: '$accounts.transactions' },
      {
        $group: {
          _id: "$accounts.transactions.localAmount.currency",
          totalAmount: { $sum: "$accounts.transactions.localAmount.amount" },
          count: { $sum: 1 }
        }
      }
    ];
    col.aggregate(query, function (err, docs) {
      response.send(docs);
    });
    db.close();
  });
});

/*****************
* Cuentas - post *
******************/
app.post('/accounts', function (req, response) {
  mongoClient.connect(url, function (err, db) {
    var col = db.collection('customer_accounts');
    var account = req.body;
    var query = { customer_id: req.headers.user };
    var updateValue = { "$push": account };
    var options = { upsert: true };
    col.updateOne(query, updateValue, options, function (err, doc) {
      console.log("Record updated");
      response.send(doc);
    });
    db.close();
  });
});


app.post('/accounts/:id/transactions', function (req, response) {
  mongoClient.connect(url, function (err, db) {
    var col = db.collection('customer_accounts');
    var transaction = req.body;
    var query = { customer_id: req.headers.user ,  accounts: { $elemMatch: { accountId: req.params.id } } };
    var updateValue = { "$push": {"accounts.$.transactions" : transaction} }
    col.updateOne(query, updateValue, function (err, doc) {
      response.send(doc);
    });
    db.close();
  });
});

/*********************
* Localizacion - get *
**********************/
app.get('/accounts/:accountId/transactions/:transactionId/locations', function (req, response) {
  // cliente para atacar la api de MLAB
  var url = urlLocationMlab + "&q={accountId: \"" + req.params.accountId + "\", transactionId: \"" + req.params.transactionId + "\"}"
  var client = requestjson.createClient(url);
  client.get('', function (err, res, body) {
    if (err) {
      response.status(500).send("Error getting location data");
    } else {
      response.send(body[0]);
    }
  });
});


// extra: paginacion en backend de movimientos:
/*
  db.customer_accounts.aggregate([
      {$match: {customer_id: "XXX"}}, 
      {$unwind: '$accounts'},
      {$match: {'accounts.accountId': 'ES1182002000000000000000000022841348XXXXXXXXX'}}, 
      {$unwind: '$accounts.transactions'}, 
      {$limit: limit + skip}, 
      {$skip : skip },
      {$group: {
      "_id": "$accounts.accountId",
      "transactions": {
          $push: "$accounts.transactions"
      }
      }}, 
      {$project: {
      "thingName": "$_id",
      "transactions": 1
  }
      }
  ])*/